﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace InternalSorts
{
    public partial class MainForm : Form
    {
        /* task 2a Вывести в алфавитном порядке сведения о видеофильмах с указанием
            киностудий, их выпустивших в заданный период времени (для периода
            задан год начала и год конца). Использовать многопутевое двухфазное
            естественное несбалансированное слияние.
          */
        List<Cinema> cinemas = new List<Cinema>();
        string PATH = "";
        static int countObj = 5;
        public MainForm()
        {
            InitializeComponent();           
        }

        private void Redraw(DataGridView dataGridView)
        {
            dataGridView.Rows.Clear();
            dataGridView.RowCount = cinemas.Count;
            for(int i = 0; i < dataGridView.RowCount; i++)
            {
                dataGridView.Rows[i].Cells[0].Value = cinemas[i].NameFilm;
                dataGridView.Rows[i].Cells[1].Value = cinemas[i].Year;
                dataGridView.Rows[i].Cells[2].Value = cinemas[i].Studio;
            }
        }

        private void RedrawSort()
        {
            FormYear formYear = new FormYear();
            formYear.ShowDialog();
            int[] years = formYear.getYears();
            Cinema cinema = new Cinema();
            cinemas.Clear();
            BinaryFormatter formatter = new BinaryFormatter();
            using (FileStream stream = File.OpenRead(PATH))
            {
                for (int i = 0; i < countObj; i++)
                {
                    cinema = (Cinema)formatter.Deserialize(stream);
                    if(cinema.Year > years[0] & cinema.Year < years[1])
                        cinemas.Add(cinema);
                }
            }
            Redraw(dataGridView2);
        }



        private void saveRandomToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Cinema cinema = new Cinema();
            saveFileDialog1.ShowDialog();
            PATH = saveFileDialog1.FileName;
            using (FileStream fileStream = File.Open(PATH, FileMode.OpenOrCreate))
            {
                BinaryFormatter formatter = new BinaryFormatter();
                for (int i = 0; i < countObj; i++)  
                {
                    Thread.Sleep(1);
                    cinema = Utils.generateObj();
                    formatter.Serialize(fileStream, cinema);                   
                }
            }
        }

        private void openFileRandomToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Cinema cinema = new Cinema();
            openFileDialog1.ShowDialog();
            PATH = openFileDialog1.FileName;
            BinaryFormatter formatter = new BinaryFormatter();
            using (FileStream stream = File.OpenRead(PATH))
            {
                for (int i = 0; i < countObj; i++)
                {
                    cinema = (Cinema)formatter.Deserialize(stream);
                    cinemas.Add(cinema);
                }
            }                
            Redraw(dataGridView1);
        }

        private void sortToolStripMenuItem_Click(object sender, EventArgs e)
        {
            openFileDialog1.ShowDialog();
            NaturalSort naturalSort = new NaturalSort();
            PATH = openFileDialog1.FileName;
            naturalSort.SortFile(PATH);
            RedrawSort();
        }

    }
}
