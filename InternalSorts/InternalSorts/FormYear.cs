﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace InternalSorts
{
    public partial class FormYear : Form
    {
        public FormYear()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox1.Text != "" && textBox2.Text != "")
                this.Close();
        }

        public int[] getYears()
        {
            int[] years = new int[2];
            years[0] = Convert.ToInt32(textBox1.Text);
            years[1] = Convert.ToInt32(textBox2.Text);
            return years;
        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            char number = e.KeyChar;
            if (!Char.IsDigit(number) && e.KeyChar != (char)Keys.Back && e.KeyChar != (char)Keys.Delete)    //если не цифра, то необрабатываем(игнорируем)
            {
                e.Handled = true;
            }
        }

        private void textBox2_KeyPress(object sender, KeyPressEventArgs e)
        {
            char number = e.KeyChar;
            if (!Char.IsDigit(number) && e.KeyChar != (char)Keys.Back && e.KeyChar != (char)Keys.Delete)    //если не цифра, то необрабатываем(игнорируем)
            {
                e.Handled = true;
            }
        }
    }
}
