﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InternalSorts
{
    public class Utils
    {
        private static string getString()
        {
            // Создаем массив букв, которые мы будем использовать.
            char[] letters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".ToCharArray();

            // Создаем генератор случайных чисел.
             Random rand = new Random();

            // Сделайте слово.
            string word = "";
            for (int j = 1; j <= 6; j++)
            {
                // Выбор случайного числа от 0 до 25
                // для выбора буквы из массива букв.
                int letter_num = rand.Next(0, letters.Length - 1);

                // Добавить письмо.
                word += letters[letter_num];
            }
            return word;
        }

        private static int getInt()
        {
            Random random = new Random();
            int n = 1900;
            return n + random.Next(0, 120);
        }

        private static bool getBool()
        {
            Random random = new Random();
            return random.Next(0, 1) == 1 ? true : false;
        }


        public static Cinema generateObj()
        {
            Cinema cinema = new Cinema();
            cinema.NameFilm = getString();
            cinema.Year = getInt();
            cinema.IsPrize = getBool();
            cinema.Director = getString();
            cinema.Studio = getString();
            for (int i = 0; i < 3; i++)
                cinema.Heroes[i] = getString();
            return cinema;
        }   
    }
}
