﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InternalSorts
{
    /* название;
 год выпуска;
 киностудия;
 режиссер;
 длительность фильма;
 наличие приза;
 три главных героя.*/

[Serializable]
    public class Cinema
    {
        public string NameFilm { get; set; }
        public int Year { get; set; }
        public string Studio { get; set; }
        public string Director { get; set; }
        public bool IsPrize { get; set; }
        public string[] Heroes { get; set; } = new string[3]; 
    }
}
